var router = require('express').Router();
var mocks = require('./mock');
var schema = require('../db/schema');
var Question = schema.models.Question;

router.get('/question', function (req, res, next) {
    Question.all(function (err, questions) {
        if (err) return next();
        res.json(questions)
    })
});

router.get('/question/:id', function (req, res, next) {
    var question = withComments(mocks.questions).filter(function (question) {
        return question.id == req.params.id
    })[0];
    if (question) return res.json(question);

    res.status(404).json({error: "not found"});
});

router.post('/question', function (req, res, next) {
    var body = req.body;
    var question = new Question({
        text: body.text,
        username: body.username
    });

    question.save(function (err, model) {
        if (err) return next(err);
        res.json(model)
    })
});

router.get('/comment', function (req, res, next) {
    var qid = req.query.question;
    Question.find(qid, function(err, question) {
        if (err) next(err);
        question.comments.all(function () {
            res.json(arguments)
        })
    })
});

router.post('/comment', function (req, res, next) {
    var qid = req.body.qid;
    var comment = {
        text : req.body.text,
        username: req.body.username
    };

    Question.find(qid, function (err, question) {
        if (err) return next(err);
        question.comments
            .build(comment)
            .save(function (err, comment) {
                if (err) return next(err);
                res.json(comment)
            })
    })
});

module.exports = router;

function withComments(questions) {
    return questions.map(function (q) {
        q.cids = mocks.comments.filter(function (comment) {
            return comment.qid == q.id
        }).map(function (comment) {
            return comment.id
        });
        return q
    })
}