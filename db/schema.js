var Schema = require('jugglingdb').Schema;

var schema = new Schema('postgres', {
    database: 'qa_app',
    username: 'roma',
    password: 'password'
});

var Question = schema.define('Question', {
    text: Schema.Text,
    username: String
}, {
    tablename: 'questions'
});

var Comment = schema.define('Comment', {
    text: Schema.Text,
    username: String
}, {
    tablename: 'comments'
});

Comment.belongsTo(Question, {as: 'question', foreignKey: 'qid'});
Question.hasMany(Comment, {as: 'comments', foreignKey: 'qid'});

schema.isActual(function(err, actual) {
    console.log('--- is actual: ', actual);
    if (!actual) {
        schema.automigrate();
    }
});

module.exports = schema