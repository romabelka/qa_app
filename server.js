var path = require('path');
var express = require('express');
var webpack = require('webpack');
var config = require('./webpack.config');
var api = require('./api');
var bodyParser = require('body-parser');
var schema = require('./db/schema');


var app = express();
var compiler = webpack(config);

app.use(bodyParser.json());
app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.use('/api', api)

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.use(function (err, req, res, next) {
    if (err) res.status(500).json({error: err})
});

app.listen(3000, 'localhost', function (err) {
    if (err) {
        console.log(err);
        return;
    }

    console.log('Listening at http://localhost:3000');
});
