import keyMirror from 'keymirror'

export default keyMirror({
    GET_QUESTIONS_START: null,
    GET_QUESTIONS_SUCCESS: null,
    GET_QUESTIONS_FAIL: null,

    POST_QUESTIONS_START: null,
    POST_QUESTIONS_SUCCESS: null,
    POST_QUESTIONS_FAIL: null,

    GET_COMMENTS_START: null,
    GET_COMMENTS_SUCCESS: null,
    GET_COMMENTS_FAIL: null,

    POST_COMMENTS_START: null,
    POST_COMMENTS_SUCCESS: null,
    POST_COMMENTS_FAIL: null,

    FILTER_QUESTIONS: null
})