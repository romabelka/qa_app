import '../../node_modules/babel-core/lib/polyfill'; // For ES7 features

import React, {Component} from 'react';

export default class IdentForm extends Component{
    static propTypes = { initialCount: React.PropTypes.number };
    static defaultProps = { initialCount: 0 };
    state={
        count:this.props.initialCount
    };
    handleClick = () => {
        this.setState({count:this.state.count + 1})
    };
    render(){
        return(
            <div>
                <button className ="button" onClick={this.handleClick}>Enter</button>
                <input type="text" className="form-control" placeholder="enter the task"/>
            </div>
        )
    }
}