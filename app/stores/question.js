import AppDispatcher from '../dispatcher/Dispatcher.js';
import constants from '../constants/constants.js';
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
const CHANGE_EVENT = 'change';
import {
    GET_QUESTIONS_START,
    GET_QUESTIONS_SUCCESS,
    GET_QUESTIONS_FAIL,
    POST_QUESTIONS_START,
    POST_QUESTIONS_SUCCESS,
    POST_QUESTIONS_FAIL,
    FILTER_QUESTIONS
} from '../constants/constants';


let data = {};

var questionStore = assign({}, EventEmitter.prototype, {
    add: function(info) {
        data[info.id] = info
    },

    getAll: function () {
        return Object.keys(data).map(function (id) {
            return data[id]
        })
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * @param {function} callback
     */
    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (action) {
    switch (action.actionType) {
        case GET_QUESTIONS_START:
            questionStore.isLoading = true;
            questionStore.emitChange();
            break;

        case GET_QUESTIONS_SUCCESS:
            action.data.response.forEach(function (question) {
                questionStore.add(question)
            });
            questionStore.isLoding = false;
            questionStore.emitChange();
            break;

        case GET_QUESTIONS_FAIL:
            questionStore.isLoding = false;
            questionStore.error = action.error;
            questionStore.emitChange();
            break;
    }
});

export default questionStore;



