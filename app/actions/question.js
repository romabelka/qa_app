import AppDispatcher from '../dispatcher/Dispatcher';
import {
    GET_QUESTIONS_START,
    GET_QUESTIONS_SUCCESS,
    GET_QUESTIONS_FAIL,
    POST_QUESTIONS_START,
    POST_QUESTIONS_SUCCESS,
    POST_QUESTIONS_FAIL,
    FILTER_QUESTIONS
} from '../constants/constants';

let TodoActionCreator = {
    getQuestions: function(){
        AppDispatcher.dispatch({
            actionType:GET_QUESTIONS_START
        });

        getQuestions()
            .done(function (response) {
                AppDispatcher.dispatch({
                    actionType: GET_QUESTIONS_SUCCESS,
                    data: {
                        response: response
                    }
                })
            })
            .fail(function (err) {
                AppDispatcher.dispatch({
                    actionType: GET_QUESTIONS_FAIL,
                    error: err
                })
            })
    }
};
